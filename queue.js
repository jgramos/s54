let collection = [];
// Write the queue functions below.

function print(){
	return collection;
}

let isEmpty = () => {
    if(collection.length == 0){
    	return true;
    } else {
    	return false;
    }
}

function enqueue(x){
    if(collection.length == 0){
        collection[0] = x;
    } else {
        collection[collection.length] = x;
    }
    return collection;
}

function dequeue(){
    let x = [];
    for(let i = 0; i < collection.length - 1; i++){
        x[i] = collection[i+1];
    }
    return collection = [...new Set(x)];
}

function front(){
    return collection[0];
}

function size(){
    return collection.length;
}

module.exports = { 
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty  
};